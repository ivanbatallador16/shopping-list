// Import React
import React, { useState, useEffect  } from 'react';

// Import Bootstrap
import { Row, Col, Container, Card, Button } from 'react-bootstrap';

// Import Child Components
import List from './List';
import SaveButton from './SaveButton';



export default function Items({ items, setItems, listName, selectedType }) {

  
  const [cardText, setCardText] = useState("");
  const [allCardsFilled, setAllCardsFilled] = useState(false);

  // Checks if all the cards are filled
  useEffect(() => {
    const filled = items.every(item => item.text.trim() !== "");
    setAllCardsFilled(filled);
  }, [items]);


  // Saves the changes to setCardText
  const handleCardTextChange = (text) => {
    setCardText(text);
  };

  // Function for Drag and Sorting the List
  const moveItem = (dragIndex, hoverIndex) => {
    const dragItem = items[dragIndex];
    setItems((prevItems) => {
      const newItems = [...prevItems];
      newItems.splice(dragIndex, 1);
      newItems.splice(hoverIndex, 0, dragItem);
      return newItems;
    });
  };


  // Saves the changes on the text field of the item
  const handleTextChange = (itemId, newText) => {
    setItems((prevItems) => {
      const updatedItems = prevItems.map((item) =>
        item.id === itemId ? { ...item, text: newText } : item
      );
      return updatedItems;
    });
  };


  // Delete the Item with a specific id
  const deleteItem = (itemId) => {
    setItems(prevItems => {
        const updatedItems = prevItems.filter(item => item.id !== itemId);
        return updatedItems;
      });
  };

  return (
    <Container fluid>
    	<Card style={{ width: '800px' }} className="Card mx-auto">
	    <Row className="m-2">
	    	<Col className="ms-5 ps-4">Item Name</Col>
	    	<Col className="ms-5 ps-5">Quantity</Col>
	    </Row>
	    </Card>
      <Row>
        <Col className="d-flex justify-content-center">
          <div>
            {/*Mapping the Items Array*/}
            {items.map((item, index) => (
              <List
                key={item.id}
                id={item.id}
                text={item.text}
                index={index}
                moveItem={moveItem}
                handleTextChange={handleTextChange}
                deleteItem={deleteItem}
                listName={listName} 
                selectedType={selectedType}
                cardText={cardText}
                handleCardTextChange={handleCardTextChange} //
              />
            ))}
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <SaveButton 
            items={items}
            listName={listName} 
            selectedType={selectedType}
            cardText={cardText}
            allCardsFilled={allCardsFilled} />
        </Col>
      </Row>
    </Container>
  );
}
