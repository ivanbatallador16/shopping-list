import React, { useRef, useState } from 'react';
import { useDrag, useDrop } from 'react-dnd';

// Import Bootstrap
import { Row, Col, Container, Card, Button } from 'react-bootstrap';

// Import React Icons 
import { BsThreeDotsVertical } from 'react-icons/bs'
import { FaTrash } from 'react-icons/fa';


// Define the types of draggable items
const ItemTypes = {
  CARD: 'card',
};

export default function List({ id, text, index, moveItem, handleTextChange, deleteItem, listName, selectedType, cardText, handleCardTextChange  }) {

	// State to manage the name of the item and selected option in the dropdown
	const [ itemName, setItemName] = useState('');
  const [selectedOption, setSelectedOption] = useState('1');
  
  // Reference for the draggable element and droppable area
  const refDrag = useRef(null); 
  const refDrop = useRef(null); 

  // Hook to enable dragging functionality using React DND
  const [{ isDragging }, drag] = useDrag({
    type: ItemTypes.CARD,
    item: { id, index },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  // Hook to enable dropping functionality using React DND
  const [, drop] = useDrop({
    accept: ItemTypes.CARD,
    hover(item) {
      if (!drag) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;
      if (dragIndex === hoverIndex) {
        return;
      }
      moveItem(dragIndex, hoverIndex);
      item.index = hoverIndex;
    },
  });

  // Attach the drag and drop functionality to the elements
  drag(refDrag);
  drop(refDrop);

  // Adjust the opacity of the card while dragging
  const opacity = isDragging ? 0.5 : 1;

	// Function to handle changes in the input field
  const handleChange = (e) => {
    const newText = e.target.value;
    setItemName(newText);
    handleTextChange(id, newText);
    handleCardTextChange(newText);
  };

  // Function to handle changes in the dropdown
  const handleDropdownChange = (e) => {
    setSelectedOption(e.target.value);
  };

  // Function to handle deletion of the item
  const handleDelete = () => {
    deleteItem(id);
  };


  return (
  	<Container>
  		<Row>
  			<Col>
				    <Card className="mt-3" ref={refDrop} style={{ width: '800px' }}>
				        <Row className="Card p-3">
				            <Col xs={1} className="d-flex align-items-center">
				                <div ref={refDrag} style={{ opacity }}>
					              <BsThreeDotsVertical style={{ cursor: 'move' }} />
					            </div>
				            </Col>
				            <Col xs={6} style={{ opacity }}>
				                <input
				                    type="text"
				                    value={itemName}
				                    className="form-control"
				                    placeholder="Enter text..."
				                    onChange={handleChange}
				                />
				            </Col>
				            <Col style={{ opacity }}>
				                <select
				                    value={selectedOption}
				                    onChange={handleDropdownChange}
				                    className="form-select"
				                >
				                    <option value="1">1</option>
				                    <option value="2">2</option>
				                    <option value="3">3</option>
				                    <option value="4">4</option>
				                    <option value="5">5</option>
				                    <option value="6">6</option>
				                    <option value="7">7</option>
				                    <option value="8">8</option>
				                    <option value="9">9</option>
				                    <option value="10">10</option>
				                    <option value="11">11</option>
				                    <option value="12">12</option>
				                </select>
				            </Col>
				            <Col xs={2}  style={{ opacity }}>
				                <Button variant="danger" onClick={handleDelete}>
				                	<FaTrash />
				                </Button>
				            </Col>
				        </Row>
				    </Card>
  			</Col>
  		</Row>
	    	
      
	</Container>
    
  );
}
