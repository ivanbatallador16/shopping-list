// Import React
import React from 'react';

//Import Bootstrap
import { Row, Col, Container, Button } from 'react-bootstrap';

//Import SweetAlert2 
import Swal from 'sweetalert2';


export default function SaveButton({ items, listName, selectedType, allCardsFilled }) {


	// Function to check if the List Name, Type, All Item Names are filled, and Items List are not empty
	const areFieldsEmpty = () => {
		if( listName === "" || selectedType === "" || allCardsFilled === false || items.length === 0 ){
			return true
		}
	} 

	// Handles the Save Button functionality
	const handleSave = () => {
			// SweetAlert2 Pop up modal
    		Swal.fire({
    		  title: 'Shopping List Saved!',
    		  icon: 'success',
    		  confirmButtonColor: '#3085d6',
    		  showClass: {
    		    popup: 'animate__animated animate__fadeInDown'
    		  },
    		  hideClass: {
    		    popup: 'animate__animated animate__fadeOutUp'
    		  }
    		})
  };

	return(
		<Container>
		      <Row className="mt-3">
			      <Col className="text-center">
			        <Button variant="success" disabled={areFieldsEmpty()} onClick={handleSave}>Save</Button>{' '}
			        <Button variant="secondary">Cancel</Button>
			      </Col>
			  </Row>
		</Container>
	)
}