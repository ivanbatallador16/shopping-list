//Bootstrap CSS
import { Row, Col, Container } from 'react-bootstrap';

//Import CSS
import './ItemList.css';

//Import Items Component
import Items from './Items';

//Import React
import { useState } from 'react';

//Import React DND
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

//Import UUID
import { v4 as uuidv4 } from 'uuid';


export default function ItemList({ listName, selectedType }) {

	const [items, setItems] = useState([]);

	// Function for Adding an Item. Generates a unique id in every Item using UUID
  const addItem = () => {
    const newItem = { id: uuidv4(), text: '' };
    setItems(prevItems => {
      const updatedItems = [...prevItems, newItem];
      return updatedItems;
    });
  };


	return(
		<Container fluid>
			{/*Add Item Button*/}
			<Row className="justify-content-center m-3"> 
				<Col xs="auto">
					<button className="addbutton" type="button" onClick={addItem}>
					  <span className="addbutton__text">Add Item</span>
					  <span className="addbutton__icon"><svg className="addsvg" fill="none" height="24" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><line x1="12" x2="12" y1="5" y2="19"></line><line x1="5" x2="19" y1="12" y2="12"></line></svg></span>
					</button>
				</Col>
			</Row>
			<Row>
				<Col>
					<DndProvider backend={HTML5Backend}>
					<Items items={items} setItems={setItems} listName={listName} selectedType={selectedType}/>
					</DndProvider>
				</Col>
			</Row>
		</Container>
	)
}