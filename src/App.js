import './App.css';

//Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';

//React Router DOM
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

//Pages
import ShoppingList from './pages/ShoppingList';

function App() {
  return (

    <Router>
      <Container fluid  className="List">
        <Routes>
           <Route path="/" element={<ShoppingList/>} />
        </Routes>
      </Container>
    </Router>
  );
}

export default App;
