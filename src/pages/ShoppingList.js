//Bootstrap CSS
import { Row, Col, Container, Form } from 'react-bootstrap';


//Import React
import { useState } from 'react';

//Import ItemList Component
import ItemList from '../components/ItemList';


export default function ShoppingList() {

	const [listName, setListName] = useState('');
  	const [selectedType, setSelectedType] = useState('');


	return(
		<Container fluid>
			<Row>
				<Col>
					<h3>My Shopping List</h3>
				</Col>
			</Row>
			<Row className = "mx-5 px-5">
				<Col>
					{/*Input Field for List Name*/}
					<Form>
				      <Form.Group className="mb-3">
				        <Form.Label>List Name</Form.Label>
				        <Form.Control type="text" value={listName} onChange={(e) => setListName(e.target.value)} required/>
				      </Form.Group>
				     </Form>
				</Col>
				<Col>
					{/*Dropdown for Type*/}
					<Form.Label>Type</Form.Label>
					<Form.Select value={selectedType} onChange={(e) => setSelectedType(e.target.value)}>
					<Form.Control required/>
				      <option></option>
				      <option value="Grocery">Grocery</option>
				      <option value="Home Goods">Home Goods</option>
				      <option value="Hardware">Hardware</option>
				    </Form.Select>
				</Col>
			</Row>
			<Row>
				<Col>
					<ItemList listName={listName} selectedType={selectedType} />
				</Col>
			</Row>
		</Container>
	)
};